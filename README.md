# Hello Disney Team!

While I didn't get as far as I had hoped, this repo contains what I have completed.

## Not completed
- Movement of the thumbnails and selector. Reason: ran out of time, but I have an idea of how to proceed this path.
- Sizing and scaling of the thumbnails. Reason: Time, but would use the matrix to scale and shift through experimentation.
- Further refactoring of main event loop into the `menu.rs` module. Reason: aimed to complete the above two tasks before doing so.

## Major libraries utilized
- Glium: OpenGL implemtation in rust
- Tokio: asynchronous runtime

## Release locations
Binaries can be downloaded from these locations. Please note that they are in a raw form and are not properyly signed and put into installers.
### Windows
- location: https://disneydemo.s3.amazonaws.com/target/win/disneydemo.exe
- md5sum: `b6e0613e75a3a4d179919ad973bd3a6b`
- node: I did not have time to propely sign this so honestly I wouldn't use it
### MacOS
- location: https://disneydemo.s3.amazonaws.com/target/macos/disneydemo
- md5sum:  `c5048863bb844818e6a31b63265e062e`
- note: I am not a registered mac developer so this requires extra permissions to run

### Developer build instructions

#### Prereqs
- Rust: https://www.rust-lang.org/learn/get-started

#### Development Mode
- Build..... `cargo build`
- Run....... `cargo run`

#### Release Mode
- Build.....  `cargo build --release`
- Run....... `cargo run --release`
