//! This module contains structs that reflect the response we get from
//! the mlb api at http://statsapi.mlb.com/api/v1/schedule
//! serde_json takes care of deseriailizing the data into elements
//!

use serde::{Deserialize, Serialize};
#[allow(non_snake_case)]
#[derive(Debug, PartialEq, Serialize, Deserialize, Clone, Default)]
pub struct MLBGameRecapImageCuts {
    pub aspectRatio: String,
    pub width: u32,
    pub height: u32,
    pub src: String,
}

#[derive(Debug, PartialEq, Serialize, Deserialize, Clone, Default)]
pub struct MLBGameRecapImage {
    pub title: String,
    pub cuts: Vec<MLBGameRecapImageCuts>,
}

#[derive(Debug, PartialEq, Serialize, Deserialize, Clone, Default)]
pub struct MLBGameRecapData {
    pub r#type: String,
    pub date: String,
    pub headline: String,
    pub blurb: String,
    pub image: MLBGameRecapImage,
}

#[derive(Debug, PartialEq, Serialize, Deserialize, Clone, Default)]
pub struct MLBGameRecap {
    pub mlb: Option<MLBGameRecapData>,
}

#[derive(Debug, PartialEq, Serialize, Deserialize, Clone, Default)]
pub struct MLBGameEditorial {
    pub recap: MLBGameRecap,
}

#[derive(Debug, PartialEq, Serialize, Deserialize, Clone, Default)]
pub struct MLBGameContent {
    pub editorial: Option<MLBGameEditorial>,
}

#[derive(Debug, PartialEq, Serialize, Deserialize, Clone, Default)]
pub struct MLBTeam {
    id: u32,
    name: String,
    link: String,
}

#[allow(non_snake_case)]
#[derive(Debug, PartialEq, Serialize, Deserialize, Clone, Default)]
pub struct MLBTeamResults {
    team: MLBTeam,
    score: Option<u16>, // Game could be postponed or still in progress
    isWinner: Option<bool>,
}

#[derive(Debug, PartialEq, Serialize, Deserialize, Clone, Default)]
pub struct MLBTeams {
    home: MLBTeamResults,
    away: MLBTeamResults,
}

#[allow(non_snake_case)]
#[derive(Debug, PartialEq, Serialize, Deserialize, Clone, Default)]
pub struct MLBGameStatus {
    abstractGameState: String,
    codedGameState: String,
    detailedState: String,
    statusCode: String,
    abstractGameCode: String,
}

#[allow(non_snake_case)]
#[derive(Debug, PartialEq, Serialize, Deserialize, Clone, Default)]
pub struct MLBGame {
    pub gamePk: u64,
    pub content: MLBGameContent,
    pub teams: MLBTeams,
    pub status: MLBGameStatus,
}

#[derive(Debug, PartialEq, Serialize, Deserialize, Clone, Default)]
pub struct MLBDate {
    //total_items: Oi64,
    pub date: String,
    pub games: Vec<MLBGame>,
}

#[derive(Debug, PartialEq, Serialize, Deserialize, Clone, Default)]
pub struct MLBSchedule {
    pub dates: Vec<MLBDate>,
}

#[derive(Debug, PartialEq, Serialize, Deserialize, Clone, Default)]
pub struct GameDisplayInfo {
    pub headline: String,  // AWAY VS HOME all caps
    pub blurb: String,     // Headline
    pub image_url: String, // maximum allowed size given AR and width
}

impl MLBSchedule {
    pub fn get_completed_games_data(
        self,
        max_width: u32,
        aspect_ratio_str: String,
    ) -> Vec<GameDisplayInfo> {
        //! Helper function to grab a copy of the list of games that have
        //! completed in the MLB Schedule
        //! Returns an empty vector if no completed games are found

        let mut games = Vec::new();

        for date in self.dates.iter() {
            for game in date.games.iter() {
                match game.content.editorial.to_owned() {
                    Some(available_content) => match available_content.recap.mlb.to_owned() {
                        Some(available_recap) => {
                            let mut largest_seen_width = 0u32;
                            let mut largest_seen_url: String = "".to_string();
                            for img in available_recap.image.cuts.iter() {
                                if img.aspectRatio == aspect_ratio_str
                                    && img.width > largest_seen_width
                                    && img.width < max_width
                                {
                                    largest_seen_url = String::from(img.src.to_owned());
                                    largest_seen_width = img.width;
                                }
                            }

                            let game_headline = format!(
                                "{} VS {}",
                                game.teams.away.team.name.to_owned(),
                                game.teams.home.team.name.to_owned()
                            );

                            games.push(GameDisplayInfo {
                                headline: game_headline,
                                blurb: available_recap.blurb.to_owned(),
                                image_url: largest_seen_url,
                            });
                        }
                        None => {}
                    },
                    None => {}
                }
            }
        }

        games
    }
}
