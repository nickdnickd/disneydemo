//! This module contains the menu state information and will handle the logic of user input
//! Maintain a selector that moves under thumbnail images
extern crate glium;

use crate::graphics;
use crate::mlb_data;

enum SelectorState {
    LeftSide,
    Middle,
    RightSide,
}

pub struct GameDisplayData {
    pub game_info: mlb_data::GameDisplayInfo,
    pub thumbnail_data: glium::texture::Texture2d,
    //pub game_vertex_data: glium::VertexBuffer<graphics::Vertex>,
}

pub struct MenuLayout {
    // state: SelectorState,
    // game_info: Vec<GameDisplayData>,
    // selector_index: u32,
    // visible_thumbnails: u32,
    pub moving_left: bool,
    pub moving_right: bool,
}

impl MenuLayout {
    pub fn process_input(&mut self, event: &glium::glutin::event::WindowEvent) {
        let input = match *event {
            glium::glutin::event::WindowEvent::KeyboardInput { input, .. } => input,
            _ => return,
        };
        let pressed = input.state == glium::glutin::event::ElementState::Pressed;
        let key = match input.virtual_keycode {
            Some(key) => key,
            None => return,
        };
        match key {
            glium::glutin::event::VirtualKeyCode::Left => {
                self.moving_left = pressed;
                println!("left pressed");
            }
            glium::glutin::event::VirtualKeyCode::Right => {
                self.moving_right = pressed;
                println!("right pressed");
            }
            glium::glutin::event::VirtualKeyCode::A => {
                self.moving_left = pressed;
                println!("A pressed");
            }
            glium::glutin::event::VirtualKeyCode::D => {
                self.moving_right = pressed;
                println!("D pressed");
            }
            _ => (),
        };
    }
}
