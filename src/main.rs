#[macro_use]
extern crate glium;
extern crate cgmath;
extern crate glium_text_rusttype as glium_text;
extern crate image;
extern crate reqwest;

use bytes::Bytes;
use futures::future::join_all;
use futures::prelude::*;
use std::collections::HashMap;
use tokio::prelude::*;

use chrono::{Datelike, Local};
use glium::glutin::dpi::PhysicalSize;
use std::time::{Duration, Instant};

pub mod graphics;
pub mod menu;
pub mod mlb_data;

// To demonstarate async usage, I will initialize the runtime explicitly
// following https://blog.logrocket.com/a-practical-guide-to-async-in-rust/
// Just a generic Result type to ease error handling for us. Errors in multithreaded
// async contexts needs some extra restrictions
type AsyncResult<T> = std::result::Result<T, Box<dyn std::error::Error + Send + Sync>>;

fn load_recap_data(
    date_string: Option<String>,
) -> Result<mlb_data::MLBSchedule, serde_json::Error> {
    // If the caller doesn't supply date_string, default to today's date
    let formatted_date = match date_string {
        Some(provided_date) => provided_date,
        None => {
            let now = Local::now();
            let local_date_string = format!("{}-{}-{}", now.year(), now.month(), now.day());
            local_date_string
        }
    };

    let url = format!("http://statsapi.mlb.com/api/v1/schedule?hydrate=game(content(editorial(recap))),decisions&date={}&sportId=1", formatted_date);
    let response = reqwest::blocking::get(&url).unwrap().text().unwrap();
    //println!("response: {:?}", response);
    serde_json::from_str(&response)
}

async fn get_url_data(url: String) -> AsyncResult<HashMap<String, Bytes>> {
    let bytes = reqwest::get(&url).await?.bytes().await?;
    let mut url2dat = HashMap::new();
    url2dat.insert(url.to_owned(), bytes);
    Ok(url2dat)
}

async fn load_async(games: Vec<mlb_data::GameDisplayInfo>) -> AsyncResult<HashMap<String, Bytes>> {
    let mut thumbnail_futures = Vec::new();
    for game in games {
        thumbnail_futures.push(tokio::spawn(async move {
            get_url_data(game.image_url.to_owned()).await.unwrap()
        }));
    }
    let dl_thumbnails = join_all(thumbnail_futures).await;
    let mut url2dat = HashMap::new();
    for dl_thumbnail in dl_thumbnails.iter() {
        match dl_thumbnail {
            Ok(good_thumbnail) => url2dat.extend(good_thumbnail.to_owned()),
            Err(e) => {}
        }
    }

    Ok(url2dat)
}

fn load_game_display_data(
    display: &glium::Display,
    games: Vec<mlb_data::GameDisplayInfo>,
) -> Vec<menu::GameDisplayData> {
    let mut game_display_data = Vec::new();

    // TODO start with a simple synchronous loadout

    let mut rt = tokio::runtime::Runtime::new().unwrap();
    let future = load_async(games.to_owned());
    let result = rt.block_on(future).unwrap();

    for game in games.iter() {
        let tn_bytes = result.get(&game.image_url).unwrap();

        let tn_texture = graphics::get_texture_from_image(display, &tn_bytes);
        game_display_data.push(menu::GameDisplayData {
            game_info: mlb_data::GameDisplayInfo {
                headline: game.headline.to_owned(),
                blurb: game.blurb.to_owned(),
                image_url: game.image_url.to_owned(),
            },
            thumbnail_data: tn_texture,
            //game_vertex_data: graphics::get_thumbnail_vertices(display)
        });
    }

    game_display_data
}

fn main() {
    #[allow(unused_imports)]
    use glium::{glutin, Surface};

    let event_loop = glutin::event_loop::EventLoop::new();
    let physical_size = PhysicalSize::new(1920, 1080);
    let default_aspect_ratio = "16:9".to_string();
    let num_thumbnails_display: u32 = 5;
    let wb = glutin::window::WindowBuilder::new().with_inner_size(physical_size);
    let cb = glutin::ContextBuilder::new();
    let display = glium::Display::new(wb, cb, &event_loop).unwrap();

    // TODO time the loading process of api results to rendered images

    let now = Instant::now();
    // Load the background image to display
    // let background_bytes =
    //     reqwest::blocking::get("http://mlb.mlb.com/mlb/images/devices/ballpark/1920x1080/1.jpg")
    //         .unwrap()
    //         .bytes()
    //         .unwrap();

    // TODO experiment with loading the file directly to save io time bytes
    let background_bytes = include_bytes!("1.jpg");
    let bg_texture = graphics::get_background_texture(&display, background_bytes.to_vec());

    let mlb_schedule: mlb_data::MLBSchedule =
        load_recap_data(Some("2020-09-01".to_string())).unwrap();

    let completed_games = mlb_schedule.get_completed_games_data(
        physical_size.width / num_thumbnails_display,
        default_aspect_ratio,
    );

    let game_display_data = load_game_display_data(&display, completed_games);

    let bg_vertices = graphics::get_background_vertices(&display);
    let bg_indices = glium::index::NoIndices(glium::index::PrimitiveType::TrianglesList);

    let tn_verticies = graphics::get_thumbnail_vertices(&display);
    let tn_indices = glium::index::NoIndices(glium::index::PrimitiveType::TrianglesList);

    // TODO move as much as I can to graphics
    // The `TextSystem` contains the shaders and elements used for text display.
    let system = glium_text::TextSystem::new(&display);

    // Creating a `FontTexture`, which a regular `Texture` which contains the font.
    // Note that loading the systems fonts is not covered by this library.
    let font = glium_text::FontTexture::new(
        &display,
        &include_bytes!("Montserrat-Regular.ttf")[..],
        70,
        glium_text::FontTexture::ascii_character_list(),
    )
    .unwrap();

    // Creating a `TextDisplay` which contains the elements required to draw a specific sentence.
    // let text = glium_text::TextDisplay::new(&system, &font, "Hello world!");

    // Finally, drawing the text is done like this:
    // let matrix = [
    //     [1.0, 0.0, 0.0, 0.0],
    //     [0.0, 1.0, 0.0, 0.0],
    //     [0.0, 0.0, 1.0, 0.0],
    //     [0.0, 0.0, 0.0, 12.0],
    // ];

    println!(
        "ms to load background and thumbnails to memory: {}",
        now.elapsed().as_millis()
    );

    let program = glium::Program::from_source(
        &display,
        graphics::VERTEX_SHADER_SRC,
        graphics::FRAGMENT_SHADER_SRC,
        None,
    )
    .unwrap();

    let selector_program = glium::Program::from_source(
        &display,
        graphics::SELECTOR_VERTEX_SHADER_SRC,
        graphics::SELECTOR_FRAGMENT_SHADER_SRC,
        None,
    )
    .unwrap();

    let mut menu_layout = menu::MenuLayout {
        moving_left: false,
        moving_right: false,
    };
    event_loop.run(move |event, _, control_flow| {
        match event {
            glutin::event::Event::WindowEvent { event, .. } => {
                menu_layout.process_input(&event);
                match event {
                    glutin::event::WindowEvent::CloseRequested => {
                        *control_flow = glutin::event_loop::ControlFlow::Exit;
                        return;
                    }
                    _ => return,
                }
            }
            glutin::event::Event::NewEvents(cause) => match cause {
                glutin::event::StartCause::ResumeTimeReached { .. } => (),
                glutin::event::StartCause::Init => (),
                _ => return,
            },

            _ => return,
        }
        let next_frame_time =
            std::time::Instant::now() + std::time::Duration::from_nanos(10_000_000_000);
        *control_flow = glutin::event_loop::ControlFlow::WaitUntil(next_frame_time);

        let mut target = display.draw();
        target.clear_color(0.0, 0.0, 1.0, 1.0);

        let bg_uniforms = uniform! {
            matrix: [
                [1.0, 0.0, 0.0, 0.0],
                [0.0, 1.0, 0.0, 0.0],
                [0.0, 0.0, 1.0, 0.0],
                [0.0 , 0.0, 0.0, 1.0f32],
            ],
            tex: &bg_texture,
        };

        let mut all_tn_uniforms = Vec::new();
        let mut game_idx = 0f32;
        for game in game_display_data.iter() {
            all_tn_uniforms.push(uniform! {
                matrix: [
                    [1.0, 0.0, 0.0, 0.0],
                    [0.0, 1.0, 0.0, 0.0],
                    [0.0, 0.0, 1.0, 0.0],
                    [-0.8 + game_idx*0.38, 0.0, 0.0, 1.0f32],
                ],
                tex: &game.thumbnail_data,
            });
            game_idx = game_idx + 1.0;
        }

        let selector_uniforms = uniform! {
            matrix: [
                [1.0, 0.0, 0.0, 0.0],
                [0.0, 1.0, 0.0, 0.0],
                [0.0, 0.0, 1.0, 0.0],
                [ -0.72 + 0.38*0.9, 0.0, 0.0, 0.9f32],
            ]
        };

        // Draw the background
        target
            .draw(
                &bg_vertices,
                &bg_indices,
                &program,
                &bg_uniforms,
                &Default::default(),
            )
            .unwrap();

        // TODO draw the selectior
        target
            .draw(
                &tn_verticies,
                &tn_indices,
                &selector_program,
                &selector_uniforms,
                &Default::default(),
            )
            .unwrap();

        // Draw the all thumbnails from the selector
        for uniforms in all_tn_uniforms.iter() {
            target
                .draw(
                    &tn_verticies,
                    &tn_indices,
                    &program,
                    uniforms,
                    &Default::default(),
                )
                .unwrap();
        }

        let text = glium_text::TextDisplay::new(&system, &font, "Hello world!");
        let text_width = text.get_width();
        //println!("Text width: {:?}", text_width);

        let matrix: [[f32; 4]; 4] = cgmath::Matrix4::new(
            2.0 / text_width,
            0.0,
            0.0,
            0.0,
            0.0,
            2.0 * (physical_size.width as f32) / (physical_size.height as f32) / text_width,
            0.0,
            0.0,
            0.0,
            0.0,
            1.0,
            0.0,
            -4.0,
            1.6,
            0.0,
            7.0f32,
        )
        .into();

        glium_text::draw(&text, &system, &mut target, matrix, (1.0, 1.0, 1.0, 1.0)).unwrap();

        target.finish().unwrap();
    });
}
