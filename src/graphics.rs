//!  This module describes the graphical primitaves of the demo

extern crate glium;
extern crate image;

use bytes::Bytes;

pub static VERTEX_SHADER_SRC: &'static str = r#"
    #version 140

    in vec2 position;
    in vec2 tex_coords;
    out vec2 v_tex_coords;

    uniform mat4 matrix;

    void main() {
        v_tex_coords = tex_coords;
        gl_Position = matrix * vec4(position, 0.0, 1.0);
    }
    "#;

pub static FRAGMENT_SHADER_SRC: &'static str = r#"
    #version 140

    in vec2 v_tex_coords;
    out vec4 color;

    uniform sampler2D tex;

    void main() {
        color = texture(tex, v_tex_coords);
    }
    "#;

pub static SELECTOR_VERTEX_SHADER_SRC: &'static str = r#"
    #version 140

    in vec2 position;

    uniform mat4 matrix;

    void main() {
        gl_Position = matrix * vec4(position, 0.0, 1.0);
    }
"#;


pub static SELECTOR_FRAGMENT_SHADER_SRC: &'static str = r#"
    #version 140

    out vec4 color;

    void main() {
        color = vec4(1.0, 1.0, 1.0, 1.0);
    }
"#;

// Vertex that maps to texture cooridnates
#[derive(Copy, Clone)]
pub struct Vertex {
    position: [f32; 2],
    tex_coords: [f32; 2],
}

implement_vertex!(Vertex, position, tex_coords);

pub fn get_texture_from_image(
    display: &glium::Display,
    image_bytes: &Bytes,
) -> glium::texture::Texture2d {
    let tn_image = image::load_from_memory(image_bytes).unwrap().to_rgba();
    let tn_image_dimensions = tn_image.dimensions();
    let tn_image = glium::texture::RawImage2d::from_raw_rgba_reversed(
        &tn_image.into_raw(),
        tn_image_dimensions,
    );
    let tn_texture = glium::texture::Texture2d::new(display, tn_image).unwrap();
    tn_texture
}

pub fn get_background_texture(
    display: &glium::Display,
    background_bytes: Vec<u8>,
) -> glium::texture::Texture2d {
    let bg_image = image::load_from_memory(&background_bytes)
        .unwrap()
        .to_rgba();
    let bg_image_dimensions = bg_image.dimensions();
    let bg_image = glium::texture::RawImage2d::from_raw_rgba_reversed(
        &bg_image.into_raw(),
        bg_image_dimensions,
    );
    glium::texture::Texture2d::new(display, bg_image).unwrap()
}

pub fn get_background_vertices(display: &glium::Display) -> glium::VertexBuffer<Vertex> {
    //implement_vertex!(Vertex, position, tex_coords);

    let bg_vertex1 = Vertex {
        position: [-1.0, -1.0],
        tex_coords: [0.0, 0.0],
    };
    let bg_vertex2 = Vertex {
        position: [-1.0, 1.0],
        tex_coords: [0.0, 1.0],
    };
    let bg_vertex3 = Vertex {
        position: [1.0, -1.0],
        tex_coords: [1.0, 0.0],
    };
    let bg_vertex4 = Vertex {
        position: [1.0, 1.0],
        tex_coords: [1.0, 1.0],
    };
    let bg_vertex5 = Vertex {
        position: [-1.0, 1.0],
        tex_coords: [0.0, 1.0],
    };
    let bg_vertex6 = Vertex {
        position: [1.0, -1.0],
        tex_coords: [1.0, 0.0],
    };
    let bg_shape = vec![
        bg_vertex1, bg_vertex2, bg_vertex3, bg_vertex4, bg_vertex5, bg_vertex6,
    ];

    let bg_vertex_buffer = glium::VertexBuffer::new(display, &bg_shape).unwrap();

    bg_vertex_buffer
}

pub fn get_thumbnail_vertices(display: &glium::Display) -> glium::VertexBuffer<Vertex> {
    let tn_vertex1 = Vertex {
        position: [-0.17, -0.17],
        tex_coords: [0.0, 0.0],
    };
    let tn_vertex2 = Vertex {
        position: [-0.17, 0.17],
        tex_coords: [0.0, 1.0],
    };
    let tn_vertex3 = Vertex {
        position: [0.17, -0.17],
        tex_coords: [1.0, 0.0],
    };
    let tn_vertex4 = Vertex {
        position: [0.17, 0.17],
        tex_coords: [1.0, 1.0],
    };
    let tn_vertex5 = Vertex {
        position: [-0.17, 0.17],
        tex_coords: [0.0, 1.0],
    };
    let tn_vertex6 = Vertex {
        position: [0.17, -0.17],
        tex_coords: [1.0, 0.0],
    };

    let tn_shape = vec![
        tn_vertex1, tn_vertex2, tn_vertex3, tn_vertex4, tn_vertex5, tn_vertex6,
    ];

    let tn_vertex_buffer = glium::VertexBuffer::new(display, &tn_shape).unwrap();
    tn_vertex_buffer
}

// create a way to use text
// https://devbits.app/x/35/draw-text-with-opengl-in-rust
// 
// 